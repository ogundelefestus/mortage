<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Permission::create(['name' => 'create-user']);
        Permission::create(['name' => 'edit-user']);
        Permission::create(['name' => 'delete-user']);
        Permission::create(['name' => 'view-employee']);

        $permission = Permission::all();
        $role1 = Role::create(['name' => 'superadmin']);
        $role1->givePermissionTo($permission);
        $user = User::create([
            'name' => 'Taiwo Taiwo',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role1);


        $role2 = Role::create(['name' => 'user']);
        $role2->givePermissionTo($permission);
        $user = User::create([
            'name' => 'Adeoye Rebbeca',
            'email' => 'users@gmail.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role2);





    }
}
