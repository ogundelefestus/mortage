<?php

namespace App\Http\Controllers;

use App\Models\ApplyLoan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplyLoanController extends Controller
{
    //


    public function showLoanApplication(){

        $data =ApplyLoan::all();
        return view('user.loans',compact('data'));
    }


    public function showLoanHistory(){

        return view('user.loan_history',['data'=>ApplyLoan::where('user_id',Auth::id())->paginate(10)]);
    }

    public function showApplyLoan(){

        return view('user.apply_loan');
    }

    public function createApplyLoan(Request $request){

        $request->validate([
            'amount' => 'required',
            'reason' => 'required'
        ]);

        $data = new ApplyLoan();
        $data->amount=$request->post('amount');
        $data->reason=$request->post('reason');
        $data->user_id = Auth::user()->id;
        if ($data->save()) {
            $user = User::role('user')->get();
            return back()->with('status', 'You have apply for loan Successfully');
        }

    }
}
