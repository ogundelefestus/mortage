<?php

namespace App\Http\Controllers;

use App\Models\ApplyGrant;
use App\Models\ApplyLoan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplyGrantController extends Controller
{


    public function showGrantHistory(){

        return view('user.grant_history',['data'=>ApplyGrant::where('user_id',Auth::id())->paginate(10)]);
    }




    public function showGrantApplication(){

        $data =ApplyGrant::all();
        return view('user.grants',compact('data'));
    }

    public function showGrant(){

        return view('user.apply_grant');
    }

    public function createGrant(Request $request){

        $request->validate([
            'amount' => 'required',
            'description' => 'required'
        ]);

        $data = new ApplyGrant;
        $data->amount=$request->post('amount');
        $data->description=$request->post('description');
        $data->user_id = Auth::user()->id;
        if ($data->save()) {
            $user = User::role('user')->get();
            return back()->with('status', 'You have apply for grant Successfully');
        }

    }
}
