<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{





    /** User **/
    public function showCreateUser(){

        return view('user.create_user');
    }


    public function showUser(){

        $data=User::role('user')->get();

        return view('user.show_user',compact('data'));
    }

    public function createUser(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required',
        ]);
        $users=$request->all();
        $users['password']=Hash::make($users['password']);
        $user=User::create($users);
        $user->assignRole(['user']);
        if($user->save()){
            return back()->with('status', 'User Created Successfully');
        }
        return back()->with('danger', 'User Not Created');
    }

    public function deleteUser(User $user){

        if($user->delete()){
            return back()->with('status','User Deleted Successfully');
        }
        return back()->with('danger','Senior Citizen Not Deleted');

    }



    /** Super Admin **/
    public function showSuperAdmin(){
        //$superadmin=User::role('superadmin')->get();

        return view('user.create_superadmin');
    }


    public function showCreateSuperAdmin(){

        $datas=User::role('superadmin')->get();

        return view('user.show_admin',compact('datas'));
    }

    public function createSuperAdmin(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required',
        ]);

        $data=$request->all();
        $data['password']= Hash::make($data['password']);
        $user= User::create($data);
        $user->assignRole(['superadmin']);
        if($user->save()){
            return back()->with('status','Super Admin Created Successfully');
        }
        return back()->with('danger','Super Admin Not Created');

    }


    public function deleteSuperAdmin(User $user){

        if($user->delete()){
            return back()->with('status','Super Admin Deleted Successfully');
        }
        return back()->with('danger','Super Admin  Not Deleted');

    }






}



