 <!-- Header Area -->
 <div class="header-area" id="headerArea">
      <div class="container">
        <!-- # Paste your Header Content from here -->
        <!-- # Header Five Layout -->
        <!-- # Copy the code from here ... -->
        <!-- Header Content -->
        <div class="header-content header-style-five position-relative d-flex align-items-center justify-content-between">
          <!-- Logo Wrapper -->
          <div class="logo-wrapper"><a href="/home"><img src="/img/download.jpg" alt=""></a></div>
          <!-- Navbar Toggler -->
          <div class="navbar--toggler" id="affanNavbarToggler" data-bs-toggle="offcanvas" data-bs-target="#affanOffcanvas" aria-controls="affanOffcanvas"><span class="d-block"></span><span class="d-block"></span><span class="d-block"></span></div>
        </div>
        <!-- # Header Five Layout End -->
      </div>
    </div>
    <!-- # Sidenav Left -->
    <!-- Offcanvas -->
    <div class="offcanvas offcanvas-start" id="affanOffcanvas" data-bs-scroll="true" tabindex="-1" aria-labelledby="affanOffcanvsLabel">
      <button class="btn-close btn-close-white text-reset" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      <div class="offcanvas-body p-0">
        <!-- Side Nav Wrapper -->
        <div class="sidenav-wrapper">
          <!-- Sidenav Profile -->
          <div class="sidenav-profile bg-gradient">
            <div class="sidenav-style1"></div>
            <!-- User Thumbnail -->
            <div class="user-profile"><img src="https://dummyimage.com/600x400/000000/fff&text=o" alt=""></div>
            <!-- User Info -->
            <div class="user-info">
              <h6 class="user-name mb-0">{{\Illuminate\Support\Facades\Auth::user()->name}}</h6><span>{{\Illuminate\Support\Facades\Auth::user()->email}}</span>
            </div>
          </div>
          <!-- Sidenav Nav -->
          <ul class="sidenav-nav ps-0">
            <li><a href="/home"><i class="bi bi-house-door"></i>Dashboard</a></li>

            @role('superadmin')
            <li><a href="#"><i class="bi bi-cart-check"></i>User Management</a>
              <ul>
                <li><a href="{{route('show.admin')}}">Create Admin</a></li>
                <li><a href="{{route('create_show.user')}}">Create User</a></li>
                <li><a href="{{route('create_show.admin')}}">Show Admin</a></li>
                <li><a href="{{route('show.user')}}">Show User</a></li>
              </ul>
            </li>
            @endrole

              @role('user')
            <li><a href="{{route('show.loanhistory')}}"><i class="bi bi-folder2-open"></i>Loan History</a></li>
            <li><a href="{{route('show.granthistory')}}"><i class="bi bi-collection"></i>Grant History </a></li>
              @endrole

            <li><a href="settings.html"><i class="bi bi-gear"></i>Settings</a></li>
            <li>
              <div class="night-mode-nav"><i class="bi bi-moon"></i>Night Mode
                <div class="form-check form-switch">
                  <input class="form-check-input form-check-success" id="darkSwitch" type="checkbox">
                </div>
              </div>
            </li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="bi bi-box-arrow-right"></i>Logout</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </li>
          </ul>
          <!-- Social Info -->
          <div class="social-info-wrap"><a href="#"><i class="bi bi-facebook"></i></a><a href="#"><i class="bi bi-twitter"></i></a><a href="#"><i class="bi bi-linkedin"></i></a></div>
          <!-- Copyright Info -->
          <div class="copyright-info">
            <p>2022 &copy; Made by<a href="#">Taiwo</a></p>
          </div>
        </div>
      </div>
    </div>
