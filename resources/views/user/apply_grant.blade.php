@extends('layouts.dashboard')

@section('content')


    <div class="page-content-wrapper py-3">
        <div class="container">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if (session('danger'))
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
        @endif
        <!-- Element Heading -->
            <div class="element-heading">
                <h6>Apply For Our Loan</h6>
            </div>
        </div>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('create.applyloan')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label class="form-label" for="exampleInputText">Amount(NGN)</label>
                            <input class="form-control" id="exampleInputText" name="amount" type="number" min="10000" placeholder="Loan Amount">
                            <p style="color: red;">Minimum amount you can apply for is NGN10000</p>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="exampleInputemail">Description (optional)</label>
                            <textarea class="form-control form-control-clicked" id="exampleTextarea1" name="description" cols="3" rows="5" required placeholder="Write something..."></textarea>
                            <p style="color: red;">State the grant you are applying for</p>
                        </div>




                        <button class="btn btn-primary w-100 d-flex align-items-center justify-content-center" type="submit">Apply
                            <svg class="bi bi-arrow-right-short" width="24" height="24" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"></path>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
