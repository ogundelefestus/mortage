
@extends('layouts.dashboard')

@section('content')

    <div class="page-content-wrapper py-3">
        <div class="container">
            <!-- Element Heading -->
            <div class="element-heading">
                <h6>Grant History</h6>
            </div>
        </div>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <table class="table mb-0">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Reason</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created_at</th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $count=>$datas)
                            <tr>
                                <th scope="row">{{$count+1}}</th>
                                <td>{{App\Models\User::find($datas->user_id)->name ?? ""}}</td>
                                <td>{{$datas->amount}}</td>
                                <td>{{$datas->reason}}</td>
                                <td><span>pending</span></td>
                                <td>{{$datas->created_at}}</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


@endsection
