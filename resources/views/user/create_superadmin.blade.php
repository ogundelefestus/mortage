@extends('layouts.dashboard')

@section('content')


<div class="page-content-wrapper py-3">
      <div class="container">

          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif

          @if (session('danger'))
              <div class="alert alert-danger">
                  {{ session('danger') }}
              </div>
      @endif
        <!-- Element Heading -->
        <div class="element-heading">
          <h6>Create Admin</h6>
        </div>
      </div>
      <div class="container">
        <div class="card">
          <div class="card-body">
            <form action="{{route('create.admin')}}" method="POST">
              @csrf
              <div class="form-group">
                <label class="form-label" for="exampleInputText">Name</label>
                <input class="form-control" id="exampleInputText" name="name" type="text" placeholder="Name">
              </div>
              <div class="form-group">
                <label class="form-label" for="exampleInputemail">Email</label>
                <input class="form-control" id="exampleInputemail" type="email" name="email" placeholder="Taiwo@gmail.com">
              </div>
              <div class="form-group">
                <label class="form-label" for="exampleInputpassword">Password</label>
                <input class="form-control" id="exampleInputpassword" name="password" type="password" placeholder="Password">
              </div>



              <button class="btn btn-primary w-100 d-flex align-items-center justify-content-center" type="submit">Save
                <svg class="bi bi-arrow-right-short" width="24" height="24" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"></path>
                </svg>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>

@endsection
