
@extends('layouts.dashboard')

@section('content')

 <div class="page-content-wrapper py-3">
      <div class="container">
        <!-- Element Heading -->
        <div class="element-heading">
          <h6>All Users</h6>
        </div>
      </div>
      <div class="container">
        <div class="card">
          <div class="card-body">
            <table class="table mb-0">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Created_at</th>
                    <th scope="col">Delete</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $count=>$datas)
                <tr>
                  <th scope="row">{{$count+1}}</th>
                  <td>{{$datas->name}}</td>
                  <td>{{$datas->email}}</td>
                  <td>{{$datas->created_at}}</td>
                    <td><a href="{{route('user.delete',$datas->id)}}">Delete</a> </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>


      @endsection
