@extends('layouts.dashboard')

@section('content')
    @role('superadmin')
    @include("partials.superadmin_home")
    @endrole

    @role('user')
    @include("partials.user_home")
    @endrole


@endsection