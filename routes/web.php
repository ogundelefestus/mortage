<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group([
    'middleware' => ['auth', 'role:superadmin'],
    'prefix'=>'user',
], function() {



//User Management SuperAdmin
    Route::get('admin', 'UserController@showSuperAdmin')->name('show.admin');
    Route::get('create_admin', 'UserController@showCreateSuperAdmin')->name('create_show.admin');
    Route::post('create/admin', 'UserController@createSuperAdmin')->name('create.admin');
    Route::get('/delete_superadmin/{user?}', 'UserController@deleteSuperAdmin')->name('admin.delete');

    //User Management SuperAdmin
    Route::get('user_show', 'UserController@showUser')->name('show.user');
    Route::get('user', 'UserController@showCreateUser')->name('create_show.user');
    Route::post('create/user', 'UserController@createUser')->name('create.user');
    Route::get('/delete_user/{user?}', 'UserController@deleteUser')->name('user.delete');

   //View Loan
    Route::get('loans', 'ApplyLoanController@showLoanApplication')->name('show.loanapplication');
    //View Loan
    Route::get('grants', 'ApplyGrantController@showGrantApplication')->name('show.grantapplication');






});


Route::group([
    'middleware' => ['auth', 'role:user'],
    'prefix'=>'user',
], function() {



//User Management SuperAdmin
    Route::get('loan_apply', 'ApplyLoanController@showApplyLoan')->name('show.loan');
    Route::post('create/loan', 'ApplyLoanController@createApplyLoan')->name('create.applyloan');

//User Management SuperAdmin
    Route::get('request_grant', 'ApplyGrantController@showGrant')->name('show.grant');
    Route::post('create/grant', 'ApplyGrantController@createGrant')->name('create.grant');
//
    Route::get('loans_history', 'ApplyLoanController@showLoanHistory')->name('show.loanhistory');

    Route::get('grants_history', 'ApplyGrantController@showGrantHistory')->name('show.granthistory');





});
